-- Main result
DROP TABLE IF EXISTS test_result;
DROP SEQUENCE IF EXISTS test_result_id_seq;
CREATE SEQUENCE test_result_id_seq;
CREATE TABLE test_result (test_result_id INTEGER NOT NULL DEFAULT nextval('test_result_id_seq'), test_id INTEGER, date_id INTEGER, runtime INTEGER, state_id INTEGER);

-- Test (info about the test itself)
DROP TABLE IF EXISTS test;
DROP SEQUENCE IF EXISTS test_id_seq;
CREATE SEQUENCE test_id_seq;
CREATE TABLE test (test_id INTEGER NOT NULL DEFAULT nextval('test_id_seq'), name VARCHAR(255) NOT NULL DEFAULT 'unknown', description TEXT, owner_id INTEGER, project_id INTEGER);

-- Project
DROP TABLE IF EXISTS project;
DROP SEQUENCE IF EXISTS project_id_seq;
CREATE SEQUENCE project_id_seq;
CREATE TABLE project (project_id INTEGER NOT NULL DEFAULT nextval('project_id_seq'), name VARCHAR(255) NOT NULL, owner_id INTEGER);

-- Test states
DROP TABLE IF EXISTS state;
DROP SEQUENCE IF EXISTS state_id_seq;
CREATE SEQUENCE state_id_seq;
CREATE TABLE state (state_id INTEGER NOT NULL DEFAULT nextval('state_id_seq'), name VARCHAR(255));

-- Dates
DROP TABLE IF EXISTS date;
DROP SEQUENCE IF EXISTS date_id_seq;
CREATE SEQUENCE date_id_seq;
CREATE TABLE date (date_id INTEGER NOT NULL DEFAULT nextval('date_id_seq'), epoch INTEGER, time DATE);

-- Meta data
DROP TABLE IF EXISTS metadata;
DROP SEQUENCE IF EXISTS metadata_id_seq;
CREATE SEQUENCE metadata_id_seq;
CREATE TABLE metadata (metadata_id INTEGER NOT NULL DEFAULT nextval('metadata_id_seq'), test_result_id INTEGER, data JSONB);

-- Users
DROP TABLE IF EXISTS users;
DROP SEQUENCE IF EXISTS user_id_seq;
CREATE SEQUENCE user_id_seq;
CREATE TABLE users (user_id INTEGER NOT NULL DEFAULT nextval('user_id_seq'), username VARCHAR(255), password VARCHAR(255), salt VARCHAR(255), api_key VARCHAR(255));

-- Tokens
DROP TABLE IF EXISTS token;
CREATE TABLE token (user_id INTEGER NOT NULL, token VARCHAR(255), expires TIMESTAMP WITH TIME ZONE DEFAULT NOW() + interval '15 minutes');
