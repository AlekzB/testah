'use strict'

const results = require('./modules/result')
const tests = require('./modules/test')

const express = require('express')
const router = express.Router()

const serverErrorCode = 500

/* GET result */
router.get('/api/result/:id', (req, res) => {
	results.get(req.params.id).then((result) => {
		res.json(result)
	}).catch((response) => {
		res.status(serverErrorCode).json(response)
	})
})

/* GET tests */
router.get('/api/test', (req, res) => {
	tests.get(1).then((testList) => {
		res.json(testList)
	}).catch((response) => {
		res.status(serverErrorCode).json(response)
	})
})

/* GET results by test */
router.get('/api/test/:id/results', (req, res) => {
	results.byTest(req.params.id).then((results) => {
		res.json(results)
	}).catch((response) => {
		res.status(serverErrorCode).json(response)
	})
})

/* POST result */
router.post('/api/result', (req, res) => {
	results.process(req.body).then((id) => {
		res.json({'success': true, 'created_id': id })
	}).catch((response) => {
		res.status(serverErrorCode).json(response)
	})
})

module.exports = router
