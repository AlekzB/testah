'use strict'

const storage = require('./storage')

/**
 * Result object structure
 * {
 *	'test': {
 *		'name': 'ping',
 *		'project': 'name'
 *	},
 *	'owner': {
 *		'userId': 0,
 *		'name': 'foo',
 *		'apiKey': 'bar'
 *	},
 *	'epoch': 1487872840,
 *	'runtime': 1,
 *	'state': 'PASSED',
 *	'metadata': {
 *		'key': 'value'
 *	}
 * }
 */

exports.process = (result) =>
	new Promise((resolve, reject) => {
		storage.addResult(result).then((id) => {
			resolve(id)
		}).catch(reject)
	})

exports.get = (id) =>
	new Promise((resolve, reject) => {
		storage.getResult(id).then((result) => {
			resolve(result)
		}).catch(reject)
	})

exports.byTest = (test) =>
	new Promise((resolve, reject) => {
		storage.getResultsByTest(test).then((results) => {
			resolve(results)
		})
	})
