'use strict'

const pg = require('pg')
const logins = require('../constants')

const pool = new pg.Pool(logins.dbInfo)
pool.on('error', function(e) {
	console.log(e)
})

exports.addResult = (result) =>
	new Promise((resolve, reject) => {
		const idPromises = [
			getTestId(result).then((id) => {
				result.testId = id
			}),
			getDateId(result).then((id) => {
				result.dateId = id
			}),
			getStateId(result).then((id) => {
				result.stateId = id
			})
		]
		Promise.all(idPromises).then(() => {
			insertResultRow(result).then((newResultId) => {
				insertMetadata(newResultId, result.metadata).then(() => {
					resolve(newResultId)
				}).catch(reject)
			}).catch(reject)
		}).catch(reject)
	})

exports.getResult = (id) =>
	new Promise((resolve, reject) => {
		const query = `WITH result_row AS (\
										SELECT * FROM test_result WHERE test_result_id=${id}\
									)\
									SELECT result_row.test_result_id AS testResultId, test.test_id AS testId, \
												test.name AS test, test.owner_id, project.name AS project, date.epoch, date.time AS date, \
												result_row.runtime, state.name AS state, metadata.data AS metadata \
									FROM result_row\
									INNER JOIN test ON (test.test_id = result_row.test_id)\
									INNER JOIN project ON (project.project_id = test.project_id)\
									INNER JOIN date ON (date.date_id = result_row.date_id)\
									INNER JOIN state ON (state.state_id = result_row.state_id)\
									INNER JOIN metadata ON (result_row.test_result_id = metadata.test_result_id)`
		pool.query(query, (err, result) => {
			if (err) {
				reject(err)
			} else {
				fillUser({ 'userId': result.rows[0].owner_id }).then((user) => {
					resolve({
						'testResultId': result.rows[0].testresultid,
						'testId': result.rows[0].testid,
						'test': {
							'name': result.rows[0].test,
							'project': result.rows[0].project
						},
						'owner': user,
						'epoch': result.rows[0].epoch,
						'date': result.rows[0].date.toUTCString(),
						'runtime': result.rows[0].runtime,
						'state': result.rows[0].state,
						'metadata': result.rows[0].metadata
					})
				})
			}
		})
	})

exports.getResultsByTest = (testId) =>
	new Promise((resolve, reject) => {
		const query = `WITH result_row AS (\
										SELECT * FROM test_result WHERE test_id=${testId}\
									)\
									SELECT result_row.test_result_id AS testResultId, test.test_id AS testId, \
												test.name AS test, test.owner_id, date.epoch, date.time AS date, result_row.runtime, state.name AS state, \
												metadata.data AS metadata, project.name AS project \
									FROM result_row\
									INNER JOIN test ON (test.test_id = result_row.test_id)\
									INNER JOIN project ON (project.project_id = test.project_id)\
									INNER JOIN date ON (date.date_id = result_row.date_id)\
									INNER JOIN state ON (state.state_id = result_row.state_id)\
									INNER JOIN metadata ON (result_row.test_result_id = metadata.test_result_id)
									ORDER BY date DESC, result_row.test_result_id DESC`
		pool.query(query, (err, result) => {
			if (err) {
				reject(err)
			} else {
				if (result.rows.length > 0) {
					fillUser({ 'userId': result.rows[0].owner_id }).then((user) => {
						const results = []
						for (const row of result.rows) {
							results.push({
								'testResultId': row.testresultid,
								'testId': row.testid,
								'test': {
									'name': row.test,
									'project': row.project
								},
								'owner': user,
								'epoch': row.epoch,
								'date': row.date.toUTCString(),
								'runtime': row.runtime,
								'state': row.state,
								'metadata': row.metadata
							})
						}
						resolve(results)
					})
				} else {
					resolve([])
				}
			}
		})
	})

exports.getTests = (project) =>
	new Promise((resolve, reject) => {
		const query = `SELECT test_id, name, description, owner_id, project_id FROM test WHERE project_id = ${project}`
		pool.query(query, (err, result) => {
			if (err) {
				reject(err)
			} else {
				resolve(result.rows.map((val) =>
					{
						'testId': val.test_id,
						'name': val.name,
						'description': val.description,
						'owner': val.owner_id,
						'projectId': val.project_id
					}
				}))
			}
		})
	})

exports.addUser = (user) =>
	new Promise((resolve, reject) => {
		const query = `INSERT INTO users (username, password, salt, api_key) VALUES ('${user.name}', '${user.hashedPass}', '${user.salt}', '${user.apiKey}')`
		pool.query(query, (err, result) => {
			if (err) {
				reject(err)
			} else {
				resolve()
			}
		})
	})

exports.getUser = (username) =>
	new Promise((resolve, reject) => {
		fillUser({'name': username}).then((user) => {
			resolve(user)
		}).catch(reject)
	})

exports.getTokenOwner = (token) =>
	new Promise((resolve, reject) => {
		const query = `SELECT users.user_id AS userId, username AS name, api_key AS apiKey FROM token INNER JOIN users ON (users.user_id = token.user_id) WHERE token.token = '${token.token}'`
		pool.query(query, (err, result) => {
			if (err) {
				reject(err)
			} else {
				resolve({
					'userId': result.rows[0].userid,
					'name': result.rows[0].name,
					'apiKey': result.rows[0].apikey
				})
			}
		})
	})

exports.addToken = (token) =>
	new Promise((resolve, reject) => {
		const query = `INSERT INTO token (user_id, token) VALUES (${token.userId}, '${token.token}')`
		pool.query(query, (err) => {
			if (err) {
				reject(err)
			} else {
				resolve()
			}
		})
	})

exports.refreshToken = (token) =>
	new Promise((resolve, reject) => {
		const query = `UPDATE token SET expires = NOW() + interval '15 minutes' WHERE token = '${token.token}'`
		pool.query(query, (err) => {
			if (err) {
				reject(err)
			} else {
				resolve()
			}
		})
	})

exports.getKeyOwner = (key) =>
	new Promise((resolve, reject) => {
		const query = `SELECT user_id AS userId, username AS name, api_key AS apiKey FROM users WHERE api_key = '${key}'`
		pool.query(query, (err, result) => {
			if (err) {
				reject(err)
			} else {
				resolve({
					'userId': result.rows[0].userid,
					'name': result.rows[0].name,
					'apiKey': result.rows[0].apikey
				})
			}
		})
	})

const fillUser = (user) =>
	new Promise((resolve, reject) => {
		const query = `SELECT users.user_id, username AS name, api_key FROM users WHERE username = '${user.name}' OR user_id = ${user.userId} OR api_key = '${user.apiKey}'`
		pool.query(query, (err, result) => {
			if (err) {
				reject(err)
			} else {
				if (result.rows.length > 0) {
					resolve({
						'userId': result.rows[0].user_id,
						'name': result.rows[0].name,
						'apiKey': result.rows[0].api_key
					})
				} else {
					reject()
				}
			}
		})
	})

const getTestId = (result) =>
	new Promise((resolve, reject) => {
		testExists(result.test.name).then((id) => {
			if (typeof id !== 'undefined') {
				resolve(id)
			} else {
				getProjectId(result.test.project, result.owner).then((projectId) => {
					const query = `INSERT INTO test (name, owner_id, project_id) VALUES ('${result.test.name}', ${result.owner.userId}, ${projectId}) RETURNING test_id`
					pool.query(query, (err, response) => {
						if (err) {
							reject(err)
						} else {
							resolve(response.rows[0].test_id)
						}
					})
				})

			}
		})
	})

const testExists = (test) =>
	new Promise((resolve, reject) => {
		const query = `SELECT test_id FROM test WHERE name='${test}'`
		pool.query(query, (err, result) => {
			if (err) {
				reject(err)
			} else {
				if (result.rows.length === 0) {
					resolve()
				} else {
					resolve(result.rows[0].test_id)
				}
			}
		})
	})

const getProjectId = (project, owner) =>
	new Promise((resolve, reject) => {
		projectExists(project, owner).then((id) => {
			if (typeof id !== 'undefined') {
				resolve(id)
			} else {
				const query = `INSERT INTO project (name, owner_id) VALUES ('${project}', ${owner.userId}) RETURNING project_id`
				pool.query(query, (err, result) => {
					if (err) {
						reject(err)
					} else {
						resolve(result.rows[0].project_id)
					}
				})
			}
		})
	})

const projectExists = (project, owner) =>
	new Promise((resolve, reject) => {
		const query = `SELECT project_id FROM project WHERE name='${project}' AND owner_id=${owner.userId}`
		pool.query(query, (err, result) => {
			if (err) {
				reject(err)
			} else {
				if (result.rows.length === 0) {
					resolve()
				} else {
					resolve(result.rows[0].project_id)
				}
			}
		})
	})

const getDateId = (result) =>
	new Promise((resolve, reject) => {
		const query = `SELECT date_id FROM date WHERE date.epoch < ${result.epoch} ORDER BY time DESC LIMIT 1`
		pool.query(query, (err, response) => {
			if (err) {
				reject(err)
			} else {
				resolve(response.rows[0].date_id)
			}
		})
	})

const getStateId = (result) =>
	new Promise((resolve, reject) => {
		stateExists(result.state).then((id) => {
			if (typeof id !== 'undefined') {
				resolve(id)
			} else {
				const query = `INSERT INTO state (name) VALUES ('${result.state}') RETURNING state_id`
				pool.query(query, (err, result) => {
					if (err) {
						reject(err)
					} else {
						resolve(result.rows[0].state_id)
					}
				})
			}
		})
	})

const stateExists = (state) =>
	new Promise((resolve, reject) => {
		const query = `SELECT state_id FROM state WHERE name='${state}'`
		pool.query(query, (err, result) => {
			if (err) {
				reject(err)
			} else {
				if (result.rows.length === 0) {
					resolve()
				} else {
					resolve(result.rows[0].state_id)
				}
			}
		})
	})

const insertResultRow = (result) =>
	new Promise((resolve, reject) => {
		const query = `INSERT INTO test_result (test_id, date_id, runtime, state_id) \
									VALUES (${result.testId}, ${result.dateId}, ${result.runtime}, ${result.stateId}) \
									RETURNING test_result_id`
		pool.query(query, (err, response) => {
			if (err) {
				reject(err)
			} else {
				resolve(response.rows[0].test_result_id)
			}
		})
	})

const insertMetadata = (resultId, data) =>
	new Promise((resolve, reject) => {
		const query = `INSERT INTO metadata (test_result_id, data) VALUES (${resultId}, '${JSON.stringify(data)}'::jsonb)`
		pool.query(query, (err, response) => {
			if (err) {
				reject(err)
			} else {
				resolve()
			}
		})
	})
