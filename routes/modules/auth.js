'use strict'

const crypto = require('crypto')
const storage = require('./storage')

exports.add = (user) =>
	new Promise((resolve, reject) => {
		checkNonExistent(user).then(() => {
			generateSalt().then((salt) => {
				user.salt = salt
				hashPassword(user).then((hashedPass) => {
					user.hashedPass = hashedPass
					resolve(user)
				})
			})
		})
	})

exports.genToken = (user) =>
	new Promise((resolve, reject) => {
		generateToken(user).then((token) => {
			storage.addToken(token).then(resolve).catch(reject)
		}).catch(reject)
	})

exports.checkKey = (key) =>
	new Promise((resolve, reject) => {
		storage.getKeyOwner(key).then((owner) => {
			resolve(owner)
		}).catch(reject)
	})

const checkNonExistent = (user) =>
	new Promise((resolve, reject) => {
		storage.getUser(user.name).then(reject).catch(resolve)
	})

const generateSalt = () =>
	new Promise((resolve) => {
		const length = 32
		resolve(crypto.randomBytes(length).toString('hex').slice(0, length))
	})

const hashPassword = (user) =>
	new Promise((resolve) => {
		const hash = crypto.createHmac('sha512', user.salt)
		hash.update(user.password)
		resolve(hash.digest('hex'))
	})

const generateToken = () =>
	new Promise((resolve) => {
		const length = 32
		resolve(crypto.randomBytes(length).toString('hex').slice(0, length))
	})
