const app = angular.module("Testah", [])
app.controller("ResultController", ['$scope', '$http', ($scope, $http) => {
	$scope.title = 'Testah'
	const load = () =>
		new Promise((resolve, reject) => {
			resolve([{'name': 'Test1', 'id': 0}, {'name': 'ping', 'id': 1}])
		})

	const loadResults = () =>
		new Promise((resolve, reject) => {
			$scope.title = `Testah - ${$scope.selectedTest.name}`
			$http.get(`/api/test/${$scope.selectedTest.id}/results`).then((data) => {
				resolve(data.data)
			})
		})

	$scope.changeResults = () => {
		loadResults().then((results) => {
			$scope.results = results
			$scope.$apply()
		})
	}

	load().then((tests) => {
		$scope.tests = tests
		$scope.$apply()
		console.log($scope.testsString)
	})

	$scope.tests = []
	$scope.results = [{
		'test': {
			'name': '',
			'project': ''
		},
		'owner': {
			'userId': 0,
			'name': '',
			'apiKey': ''
		},
		'epoch': 0,
		'date': '',
		'runtime': 0,
		'state': '',
		'metadata': {
			'key': 'value'
		}
	}]
}])
